import React from 'react';
import Card from './component/card';
import './App.css';

class App extends React.Component {
  state = {
    count: 0,
    data: [],
  };

  handleSubmit = (event) => {
    event.preventDefault();
    this.setState({
      count: this.state.count + 1,
      data: [...this.state.data, event.target.task.value],
    });
    event.target.task.value = '';
  };

  render() {
    return (
      <div className="app">
        <h1>Activity Tracker</h1>
        <form onSubmit={this.handleSubmit}>
          <input
            type="text"
            placeholder="Enter your task"
            name="task"
            required
          />
          <input type="submit" value="Submit" />
        </form>
        <div>
          {this.state.count >= 1
            ? this.state.data.map((task, index) => (
                <Card key={task + ' ' + index} index={index} task={task} />
              ))
            : ''}
        </div>
      </div>
    );
  }
}

export default App;
