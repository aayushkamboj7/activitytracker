import React from 'react';
import '../styles/card.css';

let dates = [];
for (let i = 1; i <= 31; i++) {
  dates.push(i);
}

class Card extends React.Component {
  state = {
    active: false,
  };
  handleClick = (event) => {
    this.setState({
      active: !this.state.active,
    });
  };
  render() {
    return (
      <div className="card">
        <h1 className="card-heading flex-50">
          {this.props.index + 1}. {this.props.task}
        </h1>
        <div className="card-month flex-50">
          {dates.map((date, index) => (
            <div
              className={`card-dates ${this.state.active ? 'active' : ''}`}
              onClick={(event) => this.handleClick(event)}
            >
              {date}
            </div>
          ))}
        </div>
      </div>
    );
  }
}

export default Card;
